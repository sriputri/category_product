// import logo from './logo.svg';
// import './App.css';
// import { List, Card } from 'antd';
import {Card, Avatar} from 'antd';
import { Button, Radio } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import {Row, Col, Divider} from 'antd';



const style = {background: '#0092ff', padding: '8px 0'};

const {Meta} = Card;




function Product() {

    const dataProduk = [
        {
            no: 1,
            name: "Sweet Memories",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "enak",
            price: "Rp. 10000",
        },
        {
            no: 2,
            name: "The Bitty Bite",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/e/temulawak_1_1.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 3,
            name: "Roeby House",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 4,
            name: "Yummio",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 5,
            name: "Nichoa Chocolate",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 6,
            name: "Awal Mula X Realfood",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 7,
            name: "Dark Chololate Cookies",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 8,
            name: "Cashew Nuts",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 9,
            name: "Granola Bar",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 10,
            name: "Puff",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 6,
            name: "Awal Mula X Realfood",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 7,
            name: "Dark Chololate Cookies",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 8,
            name: "Cashew Nuts",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 9,
            name: "Granola Bar",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 10,
            name: "Puff",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        }
    ]

    return (
        <div style={{paddingTop:120, height: "100%", width: "100%", align: "center", backgroundColor: "#EFF1DB"}}>
            <h1 style={{fontFamily:"cursive", fontSize:"30px", margin:"0 auto", display: 'flex', justifyContent:"center"}}>Produk</h1>
            <div style={{border:"1px",borderRadius:"5px", width:"90%", height:"100%", background: "#EFF1DB", margin:"0 auto"}}>
               <div>
                   <div style={{backgroundColor:"#EFF1DB"}}>
                       <Row>
                           {dataProduk.map((a)=>{
                               return(
                                   <Col xs={12} sm={12} md= {6} lg={6} span={12} style={{paddingTop:"25px"}}>
                                       <Card
                                           hoverable
                                           style={{ width: "90%",height:"100%", borderRadius:"15px"}}
                                           cover={<img alt="example" src={a.image} />}
                                       >
                                           <Meta title={a.name} description={a.price} />
                                           <div style={{paddingTop:20, display:"flex", justifyContent:"center"}}>
                                               <Button  style={{backgroundColor:"#E08F62", color:"white"}} shape="round" icon={<PlusOutlined />}  >
                                                   Keranjang
                                               </Button>
                                           </div>
                                       </Card>
                                   </Col>
                               )
                           })}
                       </Row>
                   </div>
               </div>
                {/*<div>*/}
                {/*<Pagination defaultCurrent={1} total={50}style={{paddingTop:30, position:"right"}} />*/}
                {/*</div>*/}
            </div>
            <div style={{paddingTop:20}}/>
        </div>
    );
}

export default Product;
