// import logo from './logo.svg';
// import './App.css';
import {Button, Carousel} from 'antd';
import {Card, Col, Row} from 'antd';
import {useEffect, useState} from "react";
import {PlusOutlined} from "@ant-design/icons";

const {Meta} = Card;


function Category() {

    const [data, setData] = useState([])
    const [dataCat, setDataCat] = useState([])
    const [dataProd, setDataProd] = useState([])
    const dataProduk = [
        {
            no: 1,
            name: "Sweet Memories",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/06a2b2d0b3b3bcee577608c878a0377c/d/u/dust_busters.png",
            description: "enak",
            price: "Rp. 10000",
        },
        {
            no: 2,
            name: "Nichoa Chocolate",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/06a2b2d0b3b3bcee577608c878a0377c/d/u/dust_busters.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 6,
            name: "Awal Mula X Realfood",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/06a2b2d0b3b3bcee577608c878a0377c/d/u/dust_busters.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 7,
            name: "Dark Chololate Cookies",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/06a2b2d0b3b3bcee577608c878a0377c/d/u/dust_busters.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 8,
            name: "Cashew Nuts",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },
        {
            no: 9,
            name: "Granola Bar",
            image: "https://media-www.awalmula.co.id/catalog/product/cache/5185178c4373ca022a3ad35ddcfd802e/t/h/the_bitty_bite.png",
            description: "not1",
            price: "Rp. 10000",
        },

    ]

    useEffect(() => {
        fetch('https://staging-am.awalmula.co.id/rest/default/V1/categories')
            .then((response) => response.json())
            .then((json) => setDataCat(json))
    }, [])

    return (
        <div style={{height: "100%", width: "100%", paddingTop: 100, align: "center", backgroundColor: "#EFF1DB"}}>
            <Carousel autoplay>
                <div>
                    <h1 style={{height: '100%', lineHeight: '160px', align: 'center'}}>
                        <img
                            src={"https://media-www.awalmula.co.id/mageplaza/bannerslider/banner/image/b/a/banner-okt-2021-06.jpg"}
                            style={{width: "80%", height: "100%", borderRadius: "15px", margin: "0 auto"}}
                        />
                    </h1>
                </div>
                <div>
                    <h1 style={{height: '100%', lineHeight: '160px', alignItems: 'center'}}>
                        <img
                            src={"https://media-www.awalmula.co.id/mageplaza/bannerslider/banner/image/b/a/banner-sep-2021.jpg"}
                            style={{width: "80%", height: "100%", borderRadius: "15px", margin: "0 auto"}}
                        />
                    </h1>
                </div>
                <div>
                    <h1 style={{height: '100%', lineHeight: '160px', alignItems: 'center'}}>
                        <img
                            src={"https://media-www.awalmula.co.id/mageplaza/bannerslider/banner/image/b/a/banner-okt-2021-04.jpg"}
                            style={{width: "80%", height: "100%", borderRadius: "15px", margin: "0 auto"}}
                        />
                    </h1>
                </div>
                <div>
                    <h1 style={{height: '100%', lineHeight: '160px', alignItems: 'center'}}>
                        <img
                            src={"https://media-www.awalmula.co.id/mageplaza/bannerslider/banner/image/b/a/banner_cordlife_slide_2_desktop-02-min.jpg"}
                            style={{width: "80%", height: "100%", borderRadius: "15px", margin: "0 auto"}}
                        />
                    </h1>
                </div>
            </Carousel>,

            <div style={{ display:"flex",justifyContent:"space-around"}}>
                <div>
                    <img src="https://media-www.awalmula.co.id/rectangle-left-okt-2021.jpg"
                         style={{width: "100%", height: "100%", borderRadius: "15px"}}
                    />
                </div>
                <div>
                    <img src="https://media-www.awalmula.co.id/rectangle-left-okt-2021.jpg"
                         style={{width: "100%", height: "100%", borderRadius: "15px"}}
                    />
                </div>
                <div >
                    <img src="https://media-www.awalmula.co.id/rectangle-left-okt-2021.jpg"
                         style={{width: "100%", height: "100%", borderRadius: "15px"}}
                    />
                </div>
            </div>

            <h1 style={{fontFamily:"cursive", fontSize:"30px", margin:"0 auto", display: 'flex', justifyContent:"center"}}>Kategori</h1>
            <div style={{
                border: "1px",
                borderRadius: "5px",
                width: "90%",
                height: "40%",
                // padding: "20px",
                background: "#EFF1DB",
                margin: "0 auto"
            }}>
                {console.log((data), " tada")}
                {console.log((dataCat), " datacat")}
                <Row >
                    {dataCat?.children_data?.[0].children_data.map((a) => {
                        return (
                            <Col span={24} style={{paddingTop: "35px"}}>
                                <Card
                                    hoverable
                                    style={{width: "100%", borderRadius: "10px", bordered: "false", backgroundColor:"#EFF1DB"}}
                                    // cover={<img alt="example" src="https://media-www.awalmula.co.id/catalog/product/cache/06a2b2d0b3b3bcee577608c878a0377c/p/u/purefyl_lavender_2.png" />}
                                >
                                    <Row>
                                        <h2>{a.name}</h2>
                                    </Row>
                                    <Row>
                                        {a.children_data.map((data) => {
                                            return (
                                                <Col xs={12} sm={12} md= {6} lg={6} span={6} style={{paddingTop: "35px"}}>
                                                    <Card
                                                        hoverable
                                                        style={{width: "85%", height:"100%", borderRadius: "10px", backgroundColor:"FDEFEF"}}
                                                    >
                                                        {/*<Row>*/}
                                                        {/*    <h3>{data.name}</h3>*/}
                                                        {/*</Row>*/}
                                                        <Row style={{justifyContent:"center"}}>
                                                            <h3>{data.name}</h3>
                                                            {<img alt="example" src="https://media-www.awalmula.co.id/catalog/product/cache/06a2b2d0b3b3bcee577608c878a0377c/s/w/sweet_memories.png"
                                                                style={{width:"100%", height:"100%"}}
                                                            />}
                                                        </Row>
                                                    </Card>
                                                </Col>
                                            )
                                        })}
                                    </Row>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            </div>

            <div style={{border:"1px",borderRadius:"5px", width:"90%", height:"100%", background: "#EFF1DB", margin:"0 auto"}}>
                <div>
                    <div style={{backgroundColor:"#EFF1DB"}}>
                        <h1 style={{paddingTop:10, fontFamily:"cursive", fontSize:"30px"}}>Product Bundling Eksklusif</h1>
                        <Row>
                            {dataProduk.map((a)=>{
                                return(
                                    <Col xs={12} sm={12} md= {6} lg={6} span={6} style={{paddingTop:"25px"}}>
                                        <Card
                                            hoverable
                                            style={{ width: "90%",height:"100%", borderRadius:"15px"}}
                                            cover={<img alt="example" src={a.image} />}
                                        >
                                            <Meta title={a.name} description={a.price} />
                                            <div style={{paddingTop:20, display:"flex", justifyContent:"center"}}>
                                                <Button  style={{backgroundColor:"#E08F62", color:"white"}} shape="round" icon={<PlusOutlined />}  >
                                                    Keranjang
                                                </Button>
                                            </div>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>
                    </div>
                </div>
            </div>
            <div style={{paddingTop:40}}/>

        </div>
    );
}

export default Category;
