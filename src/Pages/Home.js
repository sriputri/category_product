// import logo from './logo.svg';
// import './App.css';
import {Carousel} from 'antd';
import {Card, Col, Row} from 'antd';
import {useEffect, useState} from "react";

const {Meta} = Card;


function Category() {

    const [data, setData] = useState([])
    const [dataCat, setDataCat] = useState([])
    useEffect(() => {
        fetch('https://staging-am.awalmula.co.id/rest/default/V1/categories')
            .then((response) => response.json())
            .then((json) => setDataCat(json))
    }, [])
    const dataCategory = [
        {
            nama: "Food & Beverages",
            sub: ["Snack", "Minuman", "Pasta & biji bijian", "dapur", "Kids Healthy"]
        },
        {
            nama: "Healt & wellnes",
            sub: ["Jaket", "Dress"]
        },
        {
            nama: "lifestyle",
            sub: ["Jaket", "Dress"]
        },
        {
            nama: "Good & Gifts",
            sub: ["Jaket", "Dress"]
        },
        {
            nama: "Promo",
            sub: ["Jaket", "Dress"]
        },
        {
            nama: "Semua Produk",
            sub: ["Jaket", "Dress"]
        },
        {
            nama: "Semua Produk",
            sub: ["Jaket", "Dress"]
        },
        {
            nama: "Semua Produk",
            sub: ["Jaket", "Dress"]
        },
        {
            nama: "Food & Beverages",
            sub: ["Snack", "Minuman", "Pasta & biji bijian", "dapur", "Kids Healthy Confort Foods"]
        },
    ]
    return (
        <div >
            <img
                src="https://media-www.awalmula.co.id/mageplaza/bannerslider/banner/image/b/a/banner-okt-2021-04.jpg"
                className="top-banner"
            />
        </div>
    );
}

export default Category;
