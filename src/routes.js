import {Switch, Route} from "react-router-dom";
import Category from "./Pages/Category";
import Product from "./Pages/Product";
import Home from "./Pages/Home";

export const Routes = () => (
    <Switch>
        <Route path="/" exact>
            <Category/>
        </Route>
        <Route path="/category">
            <Category/>
        </Route>
        <Route path="/product">
            <Product/>
        </Route>
        {/*<Route path="/tentang">*/}
        {/*    <Product/>*/}
        {/*</Route>*/}
    </Switch>
)

export default Routes;