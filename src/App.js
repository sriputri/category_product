import logo from './logo.svg';
import {BrowserRouter, Link, Route} from "react-router-dom";
import './App.css';
import { useState } from 'react';
import Routes from "./routes";
import { Row, Col, Button, Divider} from 'antd';
import {Menu} from "antd";
import {PhoneOutlined} from "@ant-design/icons";
import call from './Asset/call-black-24-dp.png';
import gmail from './Asset/gmail.png';
import fb from './Asset/facebook.png';
import twt from './Asset/twitter.png';
import inst from './Asset/instagram.png';

import {MenuFoldOutlined} from "@ant-design/icons";


const {SubMenu} = Menu;

function App() {

  const [isNavbarClicked, setIsNavbarClicked] = useState(false)
  return (
      <BrowserRouter>
        <div>
            <nav className="navbar">
                <img src="https://static-www.awalmula.co.id/version1632930498/frontend/Magento/awalmula/id_ID/images/awalmula-logo-beta.png" className="nav-logo" onClick={() => setIsNavbarClicked(false)}></img>
                <ul className={isNavbarClicked ? 'nav-links active' : 'nav-links'}>
                    <li className="nav-item">
                        <Link to={"/category"} className="nav-link" onClick={() => setIsNavbarClicked(false)} >Kategori</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={"/product"} className="nav-link" onClick={() => setIsNavbarClicked(false)}>Produk</Link>
                    </li>
                    {/*<li className="nav-item">*/}
                    {/*<Link to={""} className="nav-link" onClick={() => setIsNavbarClicked(false)} >Keranjang</Link>*/}
                    {/*</li>*/}
                    {/*<li className="nav-item">*/}
                    {/*    <Link to={""} disabled className="nav-link" onClick={() => setIsNavbarClicked(false) }>Hubungi kami</Link>*/}
                    {/*</li>*/}
                </ul>
                <div onClick={() => setIsNavbarClicked(!isNavbarClicked)} className="nav-icon">
                    <MenuFoldOutlined/>
                </div>
            </nav>

            {/*<img src="https://static-www.awalmula.co.id/version1632930498/frontend/Magento/awalmula/id_ID/images/awalmula-logo-beta.png" className="nav-banner" onClick={() => setIsNavbarClicked(false)}></img>*/}

            <section>
                <Routes/>
            </section>

            <footer>
                <h3 className="subtitleHubungiKami">Hubungi Kami</h3>
                <Row className="rowColContact">
                    <Col xs={{span: 24}} sm={{span: 24}} md={{span: 12}} lg={{span: 12}} xl={{span: 12}}>
                        {/*<img src={logo} className="logoOcbc" />*/}
                        <div className="medsosGrid">
                            <img src={call} className="callLogo" />
                            <p className="listText">Telepon  <br /> <b>+62 811-3223-9709</b></p>
                        </div>
                        <div className="medsosGrid">
                            <img src={gmail} className="callLogo" />
                            <p className="listText">E-mail <br /> <b>customercare@awalmula.co.id</b></p>
                        </div>
                        <p className="listText">Media Sosial</p>
                        <div className="medsosGrid">
                            <a href="https://www.facebook.com/CariParkirOfficial/">
                                <img src={fb} className="medsosLogo"/>
                            </a>
                            <a href="https://twitter.com/Cariparkir_ID">
                                <img src={twt} className="medsosLogo"/>
                            </a>
                            <a href="https://www.youtube.com/channel/UC5boKDW1tnU2-qgq06cvalQ">
                                <img src={inst} className="medsosLogo"/>
                            </a>
                        </div>

                    </Col>
                    <Col xs={{span: 24}} sm={{span: 24}} md={{span: 12}} lg={{span: 12}} xl={{span: 12}}>
                        {/*<img src="https://static-www.awalmula.co.id/version1632930498/frontend/Magento/awalmula/id_ID/images/awalmula-logo-beta.png" className="logoOcbc" />*/}
                        <h3>PT. Eling Pertama</h3>
                        <p className="listText">Alamat Jakarta : Business Park Kebon Jeruk Blok D1-10
                            Jl. Meruya Ilir Raya No.88, Meruya Utara, Kembangan
                            Jakarta Barat, Jakarta 11620</p>
                        <p className="listText">Alamat Surabaya : Spazio Lt. 6 Unit 617
                            Jl. Mayjen Yono Suwoyo No Kav. 3 Pradah Kalikendal, Dukuh Pakis
                            Kota Surabaya, Jawa Timur</p>
                        <div className="medsosGrid">
                            {/*<a href="https://www.facebook.com/bankocbcnisp">  <img src={fb} className="medsosLogo"/></a>*/}
                            {/*<a href="https://twitter.com/bankocbcnisp">  <img src={tw} className="medsosLogo"/></a>*/}
                            {/*<a href="https://www.youtube.com/channel/UCbtq91LPXre0uBNLOJO6VOg "><img src={yt} className="medsosLogo"/></a>*/}
                            {/*<a href="https://instagram.com/ocbc_nisp?igshid=suig99oghzh9"> <img src={ig} className="medsosLogo"/></a>*/}
                        </div>
                    </Col>
                </Row>
                <Divider style={{ color: "#b7bac2", height: "1px" }} />
                <div className="footer">
                    <p>© 2021 Technical Test.</p>
                </div>
            </footer>
        </div>
      </BrowserRouter>

  );
}

export default App;
